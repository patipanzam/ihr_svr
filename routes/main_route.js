/**
 * Created by patipan on 03/08/2018 
 */

var express = require('express');
var crypto = require('crypto');
var main_router = express.Router();
//var MLogin = require('../models/MLogin');

const Sequelize = require('sequelize');
// var db_ou = require('../dbconfig/db_ou');
var db_ihr = require('../dbconfig/db_ihr');
var db_config = require('../dbconfig/db.config');
var province = require('../model/db_province_Model');
var company = require('../model/company_Model');
var branch = require('../model/branch_Model');
var resign_reason = require('../model/resign_reason_Model');

const Op = Sequelize.Op;

main_router.get('/getprovince', function(req, res, next) {
    province.findAll({
        attributes: ['PROVINCE_ID','PROVINCE_CODE','PROVINCE_NAME'],
    }).then(function(province) {
        res.json(province);
    });
});

main_router.get('/getcompany', function(req, res, next) {
    company.findAll({
        attributes: ['company_id','company_name'],
    }).then(function(province) {
        res.json(province);
    });
});

main_router.get('/getbranch', function(req, res, next) {
    branch.findAll({
        attributes: ['branch_id','branch_name'],
    }).then(function(province) {
        res.json(province);
    });
});


main_router.get('/getresign', function(req, res, next) {
    resign_reason.findAll({
        attributes: ['resign_reason_id','resign_reason_code','resign_reason_name'],
    }).then(function(resign) {
        res.json(resign);
    });
});




module.exports = main_router;