/**
 * Created by patipan on 03/08/2018 
 */

var express = require('express');
var crypto = require('crypto');
var employee_router = express.Router();
var cors = require('cors');
const Sequelize = require('sequelize');

var db_ihr = require('../dbconfig/db_ihr');
var db_config = require('../dbconfig/db.config');
var employee = require('../model/employee_Model');

const Op = Sequelize.Op;


// save employee
employee_router.post('/saveemployee', cors(), function(req, res) {
    var obj = req.body;
    var emp_code = obj.emp_code;
    console.log(emp_code)
    if (emp_code) {
        var sql = "SELECT emp_code FROM `employees` WHERE emp_code= '" + emp_code + "'";
        return db_ihr.transaction({ isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED }, function(t) {
            
            return db_ihr.query(sql, { type: db_ihr.QueryTypes.SELECT }).then(function(emp_code) {
                return emp_code;
            }, { transaction: t }).then(function(code) { //find emp_code
                var empcode = code.length;
                var objEmployee = {};
                if (empcode > 0) {
                var checkEmpStatusCode = 1;
                objResp = {
                    message: 'รหัสพนักงานซ้ำ',
                    checkEmpStatusCode: checkEmpStatusCode,
                };
                res.json(objResp);
                }else{

                    objEmployee.emp_code = obj.emp_code;
                    objEmployee.emp_first_name = obj.emp_first_name;
                    objEmployee.emp_last_name  = obj.emp_last_name;
                    objEmployee.emp_birth_date = obj.emp_birth_date;
                    objEmployee.emp_address = obj.emp_address
                    objEmployee.emp_address_province_id = obj.emp_address_province_id;
                    objEmployee.emp_company_id = obj.emp_company_id;
                    objEmployee.emp_branch_id = obj.emp_branch_id;
                    objEmployee.emp_work_date_start = obj.emp_work_date_start;
                    objEmployee.emp_salary = obj.emp_salary;

                    return employee.create(objEmployee, { transaction: t }).catch(function(err) {
                        console.log('error');
                        console.log(err.parent.sqlMessage);
                    });
                }
            });

        }).then(function(option) {
            var checkEmpStatusCode = 0;
            objResp = {
                message: 'บันทึกข้อมูลสำเร็จ',
                checkEmpStatusCode: checkEmpStatusCode,
                emp_id:option.emp_id
            };
            res.json(objResp);
        }).catch(function(err) {
            console.log(err);
            objError = {
                message: 'ผิดพลาด กรุณาติดต่อผู้ดูแลระบบ'
            };
            res.json(objError);
        });
    }
});

// get all employee
employee_router.get('/getemployee', function(req, res, next) {
    var _terms = req.query.inputSearch;
    employee.findAll({
        attributes: ['emp_id','emp_code','emp_first_name','emp_last_name'],
        where: {
            [Op.or]: [{
                emp_code: {
                    [Op.like]: '%' + _terms + '%'
                }
            }, {
                emp_first_name: {
                    [Op.like]: '%' + _terms + '%'
                }
            }, {
                emp_last_name: {
                    [Op.like]: '%' + _terms + '%'
                }
            }]
        }
    }).then(function(employee) {
        res.json(employee);
    }).catch(function(err) {
        res.json(err);
    });
});

//search employee by id
employee_router.get('/searchemployee', function(req, res, next) {
    var _emp_id = req.query.emp_id;
    employee.findOne({
        attributes: ['emp_id','emp_code','emp_first_name','emp_last_name','emp_birth_date','emp_address','emp_address_province_id','emp_company_id','emp_branch_id','emp_work_date_start','emp_salary'],
        where: {
            emp_id: _emp_id,
        }
    }).then(function(employee) {
        res.json(employee);
    }).catch(function(err) {
        res.json(err);
    });
});

//delete employee by id
employee_router.delete('/deleteemployee', function(req, res) {
    var _emp_id = req.query.emp_id;
        if (_emp_id) {
            db_ihr.transaction(function(t) {
                return employee.destroy({
                    where: {
                        emp_id: _emp_id
                    }
                }, { transaction: t });
            }).then(function(result) {
                objResp = {
                    effect: result,
                    message: 'Delete successfull'
                };
                res.json(objResp);
            }).catch(function(err) {
                res.json(err);
            });
        } else {
            objResp = {
                effect: 0,
                message: '404 Model Not found'
            };
            res.json(objResp);
        }
});

//updaye data employee
employee_router.put('/updateemployee', function(req, res) {
    var obj = req.body;
    var _emp_id = obj.emp_id;
    db_ihr.transaction(function(t) {
        return employee.update(obj, {
            where: {
                emp_id: _emp_id
            }
        }, { transaction: t });

    }).then(function(result) {
        console.log(result)
        if (result[0] == 1) {
            console.log('result')
            var checkEmpStatusCode = 0;
            objResp = {
                message: 'บันทึกข้อมูลสำเร็จ',
                checkEmpStatusCode: checkEmpStatusCode,
                emp_id:_emp_id
            };
            res.json(objResp);
        } else {
            res.json({ effect: 0, message: '404 Model Not found' });
        }
    }).catch(function(err) {
        res.json(err);
    });

});



module.exports = employee_router;