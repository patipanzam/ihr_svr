/**
 * Created by patipan on 03/08/2018 
 */

/*** Please understand Express GET params *****
 *
 * They have 2 method for request params from URL
 * 1. IF you use .get('/login/:username', function (req, res, next) {
 * ==> You can use  req.params.username  for get value username from URL
 * NOTE, URL format  http://localhost:3000/Login/login/อดิเทพ
 *
 * 2. IF you use get('/login', function (req, res, next) {
 * ==> You can use  req.query.username  for get value username from URL
 * NOTE, URL format  http://localhost:3000/Login/login?username=อดิเทพ
 *
 * https://stackoverflow.com/questions/6912584/how-to-get-get-query-string-variables-in-express-js-on-node-js
 */

//This Project use Express params Method 2

var express = require('express');
var crypto = require('crypto');
var login_router = express.Router();
//var MLogin = require('../models/MLogin');

const Sequelize = require('sequelize');
// var db_ou = require('../dbconfig/db_ou');
var db_ihr = require('../dbconfig/db_ihr');
var db_config = require('../dbconfig/db.config');


const Op = Sequelize.Op;
const saltkey = "momay2520pttamon";


/*
 GET Position PARAMS
 INPUT username : string
 OUTPUT JSON position_code, position_name
 */
/*
login_router.get('/getposition', function(req, res, next) {
    if (req.query.username) {
        var _username = req.query.username;
        var objParams = {
            username: _username,
        };

        MLogin.getPosition(objParams, function(err, rows) {
            if (err) {
                res.json(err);
            } else {
                res.json(rows);
            }
        });
    }
});
*/

//using ORM
login_router.get('/getprovince', function(req, res, next) {
    console.log('Login')
    // if (req.query.username) {
    //     var _username = req.query.username;
        var sql = '';
        sql = sql + " SELECT *";
        sql = sql + " FROM " + db_config.cfg_db_ihr + ".db_province";
        db_ihr.query(sql, { type: db_ihr.QueryTypes.SELECT }).then(function(login) {
            res.json(login);
        });
    // }
    // res.json('IHR Rest API');
});


/*
 POST checklogin PARAMS
 INPUT user_name : string, password : string , position_code : string
 OUTPUT true , false
 */

/*
login_router.get('/checklogin', function(req, res, next) {
    if (req.query.username && req.query.password && req.query.position) {
        var _usr = req.query.username;
        var _pwd = req.query.password;
        var _pos = req.query.position;
        var objParams = {
            username: _usr,
            password: _pwd,
            position: _pos,
        };

        MLogin.checkLogin(objParams, function(err, rows) {
            if (err) {
                res.json(err);
            } else {
                res.json(rows);
            }
        });
    }
});
*/

// login_router.get('/checklogin', function(req, res) {
//     if (req.query.username && req.query.password && req.query.position) {
//         var _usr = req.query.username;
//         var _pwd = req.query.password;
//         var _pos = req.query.position;
//         var objParams = {
//             username: _usr,
//             password: _pwd,
//             position: _pos,
//         };

//         //var hashA = crypto.createHash('sha1').update(_pwd).digest("hex");


//         Empdata.findOne({
//             where: {
//                 username: _usr,
//             }
//         }).then(function(emp) {

//             //res.json(emp);
//             if (emp) {
//                 var dbPwdHash = emp.password;
//                 var dbUser = emp.username;
//                 var dbCode = emp.Code;

//                 var hashA = crypto.createHash('sha1').update(_pwd).digest("hex");
//                 var pos = dbPwdHash.substr(-2);
//                 var stype = dbPwdHash.substr(-3, 1); // n or b
//                 var slen = (stype == 'n') ? 40 : 32;

//                 var beforesalt = dbPwdHash.substr(0, pos);
//                 var lensaltA = parseInt(pos) + parseInt(slen);
//                 var aftersaltA = dbPwdHash.substr(lensaltA);

//                 var wlen = aftersaltA.length;
//                 var marker = aftersaltA.length - 3;
//                 var altersalt = aftersaltA.substr(0, marker);

//                 var asaltLen = (-(altersalt.length)) - 3;
//                 var posLen = parseInt(pos);

//                 var wholeStrlen = dbPwdHash.substr(posLen);
//                 var Ylen = -altersalt.length - 3;
//                 var strY = dbPwdHash.substr(Ylen);

//                 var marker2 = dbPwdHash.length - strY.length;
//                 var finalSalt = dbPwdHash.substring(posLen, marker2);

//                 var salt = (stype == 'n') ? crypto.createHash('sha1').update(saltkey).digest("hex") : crypto.createHash('md5').update(saltkey).digest("hex");

//                 var unsalted = beforesalt + altersalt;

//                 if ((hashA == unsalted) && (salt == finalSalt)) {
//                     res.json(1);
//                 } else {
//                     res.json(0);
//                 }

//             } else {
//                 res.json(2);
//             }
//         });


//     }
// });



/*** Example **/
/*
//Method 1
login_router.get('/testlogin1/:username', function (req, res, next) {
    res.send('username =>'+req.params.username);
});

//Method 2
login_router.get('/testlogin2', function (req, res, next) {
    var _username = req.query.username;
    res.send('username =>'+_username);
});
*/


module.exports = login_router;