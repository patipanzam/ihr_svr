/**
 * Created by patipan on 03/08/2018 
 */

var db_ihr = require('../dbconfig/db_ihr');
const Sequelize = require('sequelize');
const resign_reason= db_ihr.define('resign_reason', {
    resign_reason_id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    resign_reason_code : { type:Sequelize.STRING(10)},
    resign_reason_name : { type:Sequelize.STRING(45)},
    status : {type: Sequelize.INTEGER},
    created_by : { type:Sequelize.STRING(45)},
    created_date : {type:Sequelize.DATE},
    updated_by : { type:Sequelize.STRING(45)},
    updated_date : {type:Sequelize.DATE},
}, {
    tableName: 'resign_reason'
});

module.exports = resign_reason;