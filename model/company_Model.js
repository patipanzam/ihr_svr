/**
 * Created by patipan on 03/08/2018 
 */

var db_ihr = require('../dbconfig/db_ihr');
const Sequelize = require('sequelize');
const company= db_ihr.define('company', {
    company_id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    company_name : { type:Sequelize.STRING(45)},
    company_address : { type:Sequelize.STRING(45)},
    company_province_id : {type: Sequelize.INTEGER},
}, {
    tableName: 'company'
});

module.exports = company;