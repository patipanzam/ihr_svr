/**
 * Created by patipan on 03/08/2018 
 */

var db_ihr = require('../dbconfig/db_ihr');
const Sequelize = require('sequelize');
const branch = db_ihr.define('branch', {
    branch_id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    branch_comp_id : {type: Sequelize.INTEGER},
    branch_name : { type:Sequelize.STRING(45)},
    branch_address : { type:Sequelize.STRING(45)},
    branch_province_id : {type: Sequelize.INTEGER},
}, {
    tableName: 'branch'
});

module.exports = branch;