/**
 * Created by patipan on 03/08/2018 
 */

var db_ihr = require('../dbconfig/db_ihr');
const Sequelize = require('sequelize');
const db_province= db_ihr.define('db_province', {
    PROVINCE_ID : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    PROVINCE_CODE : { type:Sequelize.STRING(10)},
    PROVINCE_NAME : { type:Sequelize.STRING(100)},
    REGION_ID : {type: Sequelize.INTEGER},
    STATUS : {type: Sequelize.INTEGER},
    VERSION : { type:Sequelize.DECIMAL(10, 0)},
    CREATED_BY : {type:Sequelize.STRING(40)},
    CREATED_DATE : {type:Sequelize.DATE},
    UPDATED_BY :  {type:Sequelize.STRING(20)},
    UPDATED_DATE : {type:Sequelize.DATE},
}, {
    tableName: 'db_province'
});

module.exports = db_province;