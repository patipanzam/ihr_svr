/**
 * Created by patipan on 03/08/2018 
 */

var db_ihr = require('../dbconfig/db_ihr');
const Sequelize = require('sequelize');
const employee = db_ihr.define('employee', {
    emp_id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    emp_code : { type:Sequelize.STRING(45)},
    emp_first_name : { type:Sequelize.STRING(45)},
    emp_last_name : { type:Sequelize.STRING(45)},
    emp_birth_date : {type:Sequelize.DATE},
    emp_address : { type:Sequelize.STRING(45)},
    emp_address_province_id : {type: Sequelize.INTEGER},
    emp_company_id : {type: Sequelize.INTEGER},
    emp_branch_id : {type: Sequelize.INTEGER},
    emp_work_date_start : {type:Sequelize.DATE},
    emp_salary : {type: Sequelize.INTEGER},
    tableName: 'employee'
});

module.exports = employee;